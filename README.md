Plugin for integration of the AdminLTE package into CakePHP
================================

Requirements
------------
* CakePHP 4.0+
* PHP 7.4+

Installation
------------
You can install this plugin into your CakePHP application using [composer](http://getcomposer.org).
The recommended way to install composer packages is:

```
composer require cakesol/cake-lte
```

or update your composer.json file as below
```
 "require": {
        "cakesol/cake-lte": "dev-master"
    },
```
Enable the plugin in your application by executing the command below. 
This would update your application’s bootstrap method, or put the $this->addPlugin('Cakesol/CakeLte'); snippet in the bootstrap for you.
```
bin/cake plugin load Cakesol/CakeLte
```

Support
-------

Please report bugs and feature request in the [issues](https://gitlab.com/cakesol/cake-lte/issues) section of this repository.


License
-------

Copyright 2020 Solutia. All rights reserved.

Licensed under the [MIT](http://www.opensource.org/licenses/mit-license.php) License. Redistributions of the source code included in this repository must retain the copyright notice found in each file.