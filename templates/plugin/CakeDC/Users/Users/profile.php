<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-2">
                        <?= $this->Html->image(
                            empty($user->avatar) ? $avatarPlaceholder : $user->avatar,
                            ['width' => '180', 'height' => '180', 'class' => 'img-circle elevation-2']
                        ); ?>
                    </dt>
                    <dd class="col-sm-10">
                        <h6 class="subheader"><?= __d('cake_d_c/users', 'Username') ?></h6>
                        <p><?= h($user->username) ?></p>
                        <h6 class="subheader"><?= __d('cake_d_c/users', 'Email') ?></h6>
                        <p><?= h($user->email) ?></p>
                    </dd>
                </dl>
            </div>
        </div>
    </div>
</div>
