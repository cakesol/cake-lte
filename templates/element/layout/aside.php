<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        <span class="brand-text font-weight-light">Klaverjasscores</span>
    </a>

    <div class="sidebar">
        <nav class="mt-2">
            <?= $this->element('layout/menu/aside');?>
        </nav>
    </div>
</aside>
