<li class="nav-item dropdown">
    <a class="nav-link" data-toggle="dropdown" href="#">
        <?= $this->Avatar->get(); ?>
    </a>
    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <a href="<?= $this->Url->build('/profile') ?>" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> <?=__d('cakesol/cake_lte', 'My Profile')?>
        </a>
        <div class="dropdown-divider"></div>
        <a href="<?= $this->Url->build('/users/change-password') ?>" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> <?= __d('cakesol/cake_lte','Change password')?>
        </a>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> <?= __d('cakesol/cake_lte','Account Setting')?>
        </a>
        <div class="dropdown-divider"></div>
        <a href="<?= $this->Url->build('/logout') ?>" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> <?= __d('cakesol/cake_lte','Logout')?>
        </a>
    </div>
</li>
