<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <li class="nav-item">
        <a class="nav-link"
            href="index.html"
        >
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>Dashboard</p>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link"
           href="charts.html"
        >
            <i class="nav-icon fas fa-trophy"></i>
            <p>Trophys</p>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link"
           href="widgets.html"
        >
            <i class="nav-icon fas fa-th"></i>
            <p>Widgets</p>
        </a>
    </li>
</ul>

