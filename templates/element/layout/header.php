<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <?= $this->element('layout/search', [], ['cache' => true]); ?>
    <ul class="navbar-nav ml-auto">
        <?= $this->element('layout/menu/top/messages', [], ['cache' => true]) ?>
        <?= $this->element('layout/menu/top/user', [], ['cache' => false]) ?>
    </ul>
</nav>
