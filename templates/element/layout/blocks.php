<?php
    $this->start('header');
    echo $this->element('layout/header');
    $this->end();

    $this->start('aside');
    echo $this->element('layout/aside');
    $this->end();

    $this->start('footer');
    echo $this->element('layout/footer');
    $this->end();

    $this->start('content-head');
    echo $this->element('layout/content-head');
    $this->end();

    $this->start('breadcrumb');
    echo $this->element('layout/breadcrumb');
    $this->end();
?>
