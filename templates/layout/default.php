<!DOCTYPE html>
<html dir="ltr">
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <!-- Custom CSS -->
    <?= $this->Html->css('../libs/fontawesome-free/css/all.min.css') ?>
    <?= $this->Html->css('adminlte.min.css') ?>

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <?= $this->Html->script('../libs/jquery/jquery.min.js');?>
</head>
<body class="hold-transition sidebar-mini">

    <div class="wrapper">
        <?= $this->element('layout/blocks') ?>
        <?= $this->fetch('header') ?>
        <?= $this->fetch('aside') ?>

        <div class="content-wrapper">

            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <?= $this->fetch('content-head') ?>
                        <?= $this->fetch('breadcrumb') ?>
                    </div>
                </div>
            </div>
            <div class="content">
                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
            </div>
        </div>

        <?= $this->fetch('footer') ?>
    </div>

    <?= $this->Html->script([
        '../libs/bootstrap/js/bootstrap.bundle.min.js',
        'adminlte.min.js',
    ]); ?>

    <?= $this->fetch('script') ?>
</body>
</html>
