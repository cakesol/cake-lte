<?php
declare(strict_types=1);

namespace Cakesol\CakeLte\Command;

use Cake\Console\Arguments;
use Cake\Command\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * CopyAssets command.
 */
class CopyAssetsCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|void|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        static::copyAssetsAdminLTE(ROOT);
        static::copyAssetsAdminLTEPlugins(ROOT);
    }
    /**
     * @param $src
     * @param $dst
     */
    public static function recursiveCopy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . DIRECTORY_SEPARATOR . $file) ) {
                    static::recursiveCopy($src .DIRECTORY_SEPARATOR. $file, $dst .DIRECTORY_SEPARATOR. $file);
                } else {
                    copy($src .DIRECTORY_SEPARATOR. $file,$dst .DIRECTORY_SEPARATOR. $file);
                }
            }
        }
        closedir($dir);
    }
    /**
     * @param $dir
     * @throws \Exception
     */
    public static function copyAssetsAdminLTE($dir) {
        $ds = DIRECTORY_SEPARATOR;
        $assetDir = "{$dir}{$ds}vendor{$ds}almasaeed2010{$ds}adminlte{$ds}dist{$ds}";
        $baseDir = "{$dir}{$ds}vendor{$ds}cakesol{$ds}cake-lte{$ds}webroot";
        static::recursiveCopy($assetDir, $baseDir);
    }

    /**
     * @param $dir
     * @throws \Exception
     */
    public static function copyAssetsAdminLTEPlugins($dir) {
        $ds = DIRECTORY_SEPARATOR;
        $assetDir = "${dir}{$ds}vendor{$ds}almasaeed2010{$ds}adminlte{$ds}plugins{$ds}";
        $baseDir = "{$dir}{$ds}vendor{$ds}cakesol{$ds}cake-lte{$ds}webroot{$ds}libs";
        static::recursiveCopy($assetDir, $baseDir);
    }


}
