<?php

namespace Cakesol\CakeLte\Console;
if (!defined('STDIN')) {
    define('STDIN', fopen('php://stdin', 'r'));
}
use Cake\Utility\Security;
use Composer\Script\Event;
use Exception;
/**
 * Provides installation hooks for when this application is installed via
 * composer. Customize this class to suit your needs.
 */
class Installer {
    /**
     * @param Event $event
     * @throws \Exception
     * @return void
     */
    public static function postAutoloadDump(Event $event) {
        $io = $event->getIO();
        $rootDir = dirname(dirname(dirname(dirname(__DIR__))));
        static::copyAssetsAdminLTE($rootDir, $io);
        static::copyAssetsAdminLTEPlugins($rootDir, $io);
    }

    /**
     * @param $src
     * @param $dst
     */
    public static function recursiveCopy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . DIRECTORY_SEPARATOR . $file) ) {
                    static::recursiveCopy($src .DIRECTORY_SEPARATOR. $file, $dst .DIRECTORY_SEPARATOR. $file);
                } else {
                    copy($src .DIRECTORY_SEPARATOR. $file,$dst .DIRECTORY_SEPARATOR. $file);
                }
            }
        }
        closedir($dir);
    }
    /**
     * @param $dir
     * @param $io
     * @throws \Exception
     */
    public static function copyAssetsAdminLTE($dir, $io) {
        $ds = DIRECTORY_SEPARATOR;
        $assetDir = "${dir}${ds}almasaeed2010${ds}adminlte${ds}dist${ds}";
        $baseDir = "${dir}${ds}webroot${ds}js${ds}";
        static::recursiveCopy($assetDir, $baseDir);
    }

    /**
     * @param $dir
     * @param $io
     * @throws \Exception
     */
    public static function copyAssetsAdminLTEPlugins($dir, $io) {
        $ds = DIRECTORY_SEPARATOR;
        $assetDir = "${dir}${ds}almasaeed2010${ds}adminlte${ds}plugins${ds}";
        $baseDir = "${dir}${ds}webroot${ds}js${ds}";
        static::recursiveCopy($assetDir, $baseDir);
    }
}